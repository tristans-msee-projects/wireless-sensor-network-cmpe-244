# Wireless-Sensor-Network-CMPE-244
CMPE 244 fall 2018 project, wireless sensor network

Projects built around project framework created by Preetpal Kang for [SJSU CMPE244 course](http://socialledge.com/)

Full Project documentation available here: [Wireless Sensor Network](http://socialledge.com/sjsu/index.php/F18:_Wireless_sensor_network)
