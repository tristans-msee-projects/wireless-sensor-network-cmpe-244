/*
 * new_bme280.h
 *
 *  Created on: Nov 25, 2018
 *      Author: halak
 */

#ifndef NEW_BME280_HPP_
#define NEW_BME280_HPP_

#define DEFAULT_SLAVE_ADDRESS (0xEC)

 #ifdef _DEBUG
 extern Serial pc;
 #define DEBUG_PRINT(...) pc.printf(__VA_ARGS__)
 #else
 #define DEBUG_PRINT(...)
 #endif
#include<stdio.h>
#include "i2c_base.hpp"
#include "i2c2.hpp"
#include "LPC17xx.h"

 class BME280
 {
 public:
     BME280();
     /** Create a BME280 instance
      *  which is connected to specified I2C pins with specified address
      *
      * @param sda I2C-bus SDA pin
      * @param scl I2C-bus SCL pin
      * @param slave_adr (option) I2C-bus address (default: 0x76)
      */
    // BME280(PinName sda, PinName sck, char slave_adr = DEFAULT_SLAVE_ADDRESS);

     /** Create a BME280 instance
      *  which is connected to specified I2C pins with specified address
      *
      * @param i2c_obj I2C object (instance)
      * @param slave_adr (option) I2C-bus address (default: 0x76)
      */
    // BME280(I2C2 &i2c_obj, char slave_adr = DEFAULT_SLAVE_ADDRESS);

     /** Destructor of BME280
      */
     //virtual ~BME280();

     /** Initializa BME280 sensor
      *
      *  Configure sensor setting and read parameters for calibration
      *
      */
     void initialize(void);

     /** Read the current temperature value (degree Celsius) from BME280 sensor
      *
      */
     float getTemperature(void);

     /** Read the current pressure value (hectopascal)from BME280 sensor
      *
00121      */
     float getPressure(void);

     /** Read the current humidity value (humidity %) from BME280 sensor
00125      *
00126      */
     float getHumidity(void);

 private:

     I2C2& i2c = I2C2::getInstance();
     I2C2         *i2c_p;
     //I2C2         &i2c;
     char        address = 0xec;
     uint16_t dig_T1;
     int16_t     dig_T2, dig_T3;
     uint16_t    dig_P1;
     int16_t     dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;
     uint16_t    dig_H1, dig_H3;
     int16_t     dig_H2, dig_H4, dig_H5, dig_H6;
     int32_t     t_fine;

 };



#endif /* NEW_BME280_HPP_ */
