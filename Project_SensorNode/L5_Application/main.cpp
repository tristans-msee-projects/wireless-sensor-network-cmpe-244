#include <stdio.h>
#include "wireless.h"
#include "FreeRTOS.h"
#include "printf_lib.h"
#include "task.h"
#include "adc0.h"
#include "math.h"
#include "new_bme280.hpp"
#include "tasks.hpp"
#include "io.hpp"
#include "examples/examples.hpp"

BME280 h;
#define PARA 116.6020682
#define PARB 2.769034857

void wireless_tx(void *p)
{
    // Initializing Wireless parameters
    const char max_hops = 0;
    uint8_t addr = 5;
    int sensor_data[5];
    mesh_packet_t pkt;

    //Air Quality Sensor parameters initialization
    uint8_t MQ_135;
    float __resistance;
    float air_quality;
    float temp;

    while(1)
    {
   if(wireless_get_rx_pkt(&pkt,portMAX_DELAY))
   {
    //Air Quality Sensor data calculation and calibration
    MQ_135 = adc0_get_reading(3);
    __resistance=((4095./(float)MQ_135) - 1.);
    temp = __resistance/44.2;
    air_quality = (PARA*pow(temp,-PARB));

    //Light Sensor data calculation
    float light_percent = LS.getPercentValue();

    //Temperature Pressure Humidity Sensor data calculation
    float temp_data = h.getTemperature();
    float pressure_data = h.getPressure();
    float humidity_data = h.getHumidity();

    //Putting all sensor data in single float array variable
    sensor_data[0] = air_quality;
    sensor_data[1] = light_percent;
    sensor_data[2] = temp_data;
    sensor_data[3] = pressure_data;
    sensor_data[4] = humidity_data;

    //Send the sensor data to receiver LED Matrix node
    wireless_send(addr, mesh_pkt_nack, &sensor_data, sizeof(sensor_data), max_hops);
    }

   //Display the sent data
    for(int i=0;i<5;i++)
    u0_dbg_printf("Data[%d]: %d\n",i,sensor_data[i]);
    u0_dbg_printf("\n");
    vTaskDelay(1000);
    }
}

int main(void)
{
    scheduler_add_task(new wirelessTask(PRIORITY_CRITICAL));
    h.initialize();                         //Initialzing the temp,pressure and humidity sensor
    LPC_PINCON->PINSEL1 |= (1 << 20);       //Air quality sensor
    adc0_init();                            //Initialze adc channel 0
    xTaskCreate(wireless_tx,(const char*)"TX",512,NULL,1,NULL);
    scheduler_start();
    return -1;
}
