//Tristan French ID:012499851
//Lab 1 GPIO LED and Switch
/*
 * GPIO_tcf.cpp
 *
 *  Created on: Sep 3, 2018
 *      Author: reldn_000
 */

#include <GPIO_tcf.h>
#include <LPC17xx.h>

GPIO_tcf::GPIO_tcf(uint8_t port_num, uint8_t pin_num)
{
    port = port_num;
    pin = pin_num;

        //Handle all ports
    switch (port){
        case 0:
            LPC_PINCON->PINSEL0 &= ~(0b11 << (2*pin));
            break;
        case 1:
            LPC_PINCON->PINSEL1 &= ~(0b11 << (2*pin));
            break;
        case 2:
            LPC_PINCON->PINSEL2 &= ~(0b11 << (2*pin));
            break;
        case 3:
            LPC_PINCON->PINSEL3 &= ~(0b11 << (2*pin));
            break;
        case 4:
            LPC_PINCON->PINSEL4 &= ~(0b11 << (2*pin));
            break;
        default:
            break;
    }


    //initialize pin as input
    setAsInput();
}

void GPIO_tcf::setAsInput()
{
    //Handle all ports
    switch (port){
        case 0:
            LPC_GPIO0->FIODIR &= ~(1 << pin);
            break;
        case 1:
            LPC_GPIO1->FIODIR &= ~(1 << pin);
            break;
        case 2:
            LPC_GPIO2->FIODIR &= ~(1 << pin);
            break;
        case 3:
            LPC_GPIO3->FIODIR &= ~(1 << pin);
            break;
        case 4:
            LPC_GPIO4->FIODIR &= ~(1 << pin);
            break;
        default:
            break;
    }
}

void GPIO_tcf::setAsOutput()
{
    switch (port){
        case 0:
            LPC_GPIO0->FIODIR |= 1 << pin;
            break;
        case 1:
            LPC_GPIO1->FIODIR |= 1 << pin;
            break;
        case 2:
            LPC_GPIO2->FIODIR |= 1 << pin;
            break;
        case 3:
            LPC_GPIO3->FIODIR |= 1 << pin;
            break;
        case 4:
            LPC_GPIO4->FIODIR |= 1 << pin;
            break;
        default:
            break;
    }
}

void GPIO_tcf::setDirection(bool output)
{
    if (output){
        setAsOutput();
    }
    else{
        setAsInput();
    }
}

void GPIO_tcf::setHigh()
{
/**
 * Should alter the hardware registers to set the pin as low
 */
    switch (port){
        case 0:
            LPC_GPIO0->FIOPIN |= 1 << pin;
            break;
        case 1:
            LPC_GPIO1->FIOPIN |= 1 << pin;
            break;
        case 2:
            LPC_GPIO2->FIOPIN |= 1 << pin;
            break;
        case 3:
            LPC_GPIO3->FIOPIN |= 1 << pin;
            break;
        case 4:
            LPC_GPIO4->FIOPIN |= 1 << pin;
            break;
        default:
            break;
    }
}

void GPIO_tcf::setLow()
{

    switch (port){
        case 0:
            LPC_GPIO0->FIOPIN &= ~(1 << pin);
            break;
        case 1:
            LPC_GPIO1->FIOPIN &= ~(1 << pin);
            break;
        case 2:
            LPC_GPIO2->FIOPIN &= ~(1 << pin);
            break;
        case 3:
            LPC_GPIO3->FIOPIN &= ~(1 << pin);
            break;
        case 4:
            LPC_GPIO4->FIOPIN &= ~(1 << pin);
            break;
        default:
            break;
    }
}

void GPIO_tcf::set(bool high)
{
    if (high){
        setHigh();
    }
    else{
        setLow();
    }
}

void GPIO_tcf::toggle()
{

    switch (port){
        case 0:
            LPC_GPIO0->FIOPIN ^= (1 << pin);
            break;
        case 1:
            LPC_GPIO1->FIOPIN ^= (1 << pin);
            break;
        case 2:
            LPC_GPIO2->FIOPIN ^= (1 << pin);
            break;
        case 3:
            LPC_GPIO3->FIOPIN ^= (1 << pin);
            break;
        case 4:
            LPC_GPIO4->FIOPIN ^= (1 << pin);
            break;
        default:
            break;
    }
}

bool GPIO_tcf::getLevel()
{
    bool level;

    switch (port){
        case 0:
            level = LPC_GPIO0->FIOPIN & (1 << pin);
            break;
        case 1:
            level = LPC_GPIO1->FIOPIN & (1 << pin);
            break;
        case 2:
            level = LPC_GPIO2->FIOPIN & (1 << pin);
            break;
        case 3:
            level = LPC_GPIO3->FIOPIN & (1 << pin);
            break;
        case 4:
            level = LPC_GPIO4->FIOPIN & (1 << pin);
            break;
        default:
            break;
    }

    return level;
}

GPIO_tcf::~GPIO_tcf(){
}

