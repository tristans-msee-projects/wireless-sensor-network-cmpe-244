/*
 * lookup.h
 *
 *  Created on: Nov 25, 2018
 *      Author: Jay
 */

#ifndef LOOKUP__H_
#define LOOKUP__H_

#include <stdint.h>
#include <stdio.h>
#include "io.hpp"
#include "examples/examples.hpp"
#include "data.hpp"
#include "printf_lib.h"

Light_Sensor lightobj = Light_Sensor::getInstance();
TemperatureSensor TEMP = TemperatureSensor::getInstance();
int _sc = 1;
struct sensor_data{
  int _air_q,
      _light,
      _temp,
      _psi,
      _hum;
};

//Structure to display the data in a 5x3 Matrix
struct values{
  uint8_t units,
          tens,
          thousands;
};

uint8_t (*temp_bar)[5][20];
uint8_t (*temp_bar_zero)[5][20];

typedef struct{
    uint8_t (*temp)[5][3];
}var;

uint8_t temp_celsius;
uint8_t brightness;

int flag = 1;
sensor_data s1[4] = {{0,0,0,0,0},
                     {0,0,0,0,0},
                     {0,0,0,0,0},
                     {0,0,0,0,0},
};

/************************************************************************************************************
 *                                           LOOKUP TABLE FOR 0-9                                           *
 ************************************************************************************************************/
uint8_t one[5][3] = {
        {0, 1, 0},
        {1, 1, 0},
        {0, 1, 0},
        {0, 1, 0},
        {1, 1, 1},
};
uint8_t two[5][3] = {
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1},
        {1, 0, 0},
        {1, 1, 1},
};
uint8_t three[5][3] = {
        {1, 1, 1},
        {0, 0, 1},
        {0, 1, 1},
        {0, 0, 1},
        {1, 1, 1},
};
uint8_t four[5][3] = {
        {1, 0, 1},
        {1, 0, 1},
        {1, 1, 1},
        {0, 0, 1},
        {0, 0, 1},
};
uint8_t five[5][3] = {
        {1, 1, 1},
        {1, 0, 0},
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1},
};
uint8_t six[5][3] = {
        {1, 1, 1},
        {1, 0, 0},
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1},
};
uint8_t seven[5][3] = {
        {1, 1, 1},
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 1},
        {0, 0, 1},
};
uint8_t eight[5][3] = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1},
};
uint8_t nine[5][3] = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 1, 1},
        {0, 0, 1},
        {1, 1, 1},
};
uint8_t zero[5][3] = {
        {1, 1, 1},
        {1, 0, 1},
        {1, 0, 1},
        {1, 0, 1},
        {1, 1, 1},
};
uint8_t bar[5][20] = {
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
};

uint8_t bar_zero[5][20] = {
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
};

/**************************************************************************************************************/

//function to select a digit to display
var lookup_switch(uint8_t display_value){
    var v;
    switch(display_value){
        case 0:
            v.temp = &zero;
            break;
        case 1:
            v.temp = &one;
            break;
        case 2:
            v.temp = &two;
            break;
        case 3:
            v.temp = &three;
            break;
        case 4:
            v.temp = &four;
            break;
        case 5:
            v.temp = &five;
            break;
        case 6:
            v.temp = &six;
            break;
        case 7:
            v.temp = &seven;
            break;
        case 8:
            v.temp = &eight;
            break;
        case 9:
            v.temp = &nine;
            break;
        default:
            v.temp = &zero;
    }
    return v;
}

uint16_t get_brightness()
{
    return (uint16_t)lightobj.getPercentValue();
}
//uint8_t get_temperature()
//{
//    return (uint8_t)TEMP.getCelsius();
//}

void get_lum_bar(int ll)
{
    uint16_t light;
    light = ll;
    light = light / 5;
    temp_bar = &bar;
    temp_bar_zero = &bar_zero;
    for (int i = 0; i < 5; i++)
        for(int j = 0; j < light; j++){
            lum1[i+3][22-j] = (*temp_bar)[i][j];
            lum2 [i][22-j] = (*temp_bar)[i][j];
        }
    for (int  k = 0; k < 5; k++)
        for(int l = 0; l < (19 - light); l++){
            lum1[k+3][l+4] = (*temp_bar_zero)[k][l];
            lum2 [k][l+4] = (*temp_bar_zero)[k][l];
        }
}

//Function to separate the unit, tens and thousands digit from the actual value
values value_extractor(uint16_t sensor_value){
    uint8_t uval, tval, thval, temp;
    temp = sensor_value % 100;
    thval = sensor_value / 100;
    uval = temp % 10;
    tval = temp / 10;
    return values{uval, tval, thval};
}

//Function to replace a portion of the matrix to display the sensor value
void matrix_value_update(sensor_data *ss)
{
    values _t = value_extractor(ss->_temp);
    values _h = value_extractor(ss->_hum);
    values _p = value_extractor(ss->_psi);
    values _aq = value_extractor(ss->_air_q);

    var th0 = lookup_switch(_t.thousands);
    var te0 = lookup_switch(_t.tens);
    var un0 = lookup_switch(_t.units);

    var th1 = lookup_switch(_h.thousands);
    var te1 = lookup_switch(_h.tens);
    var un1 = lookup_switch(_h.units);

    var th2 = lookup_switch(_p.thousands);
    var te2 = lookup_switch(_p.tens);
    var un2 = lookup_switch(_p.units);

    var th3 = lookup_switch(_aq.thousands);
    var te3 = lookup_switch(_aq.tens);
    var un3 = lookup_switch(_aq.units);

    for (uint8_t i = 0; i < 5; i++) {
        for (uint8_t j = 0; j < 3; j++) {
            if(_t.thousands == 0)
                temperature[i + 1][j + 6]  = 0;
            else
                temperature[i + 1][j + 6]  = (*th0.temp)[i][j];
            temperature[i + 1][j + 10] = (*te0.temp)[i][j];
            temperature[i + 1][j + 14] = (*un0.temp)[i][j];
            temp_celsius = ss->_temp;

            if(_h.thousands == 0)
                humidity[i + 1][j + 7]  = 0;
            else
                humidity[i + 1][j + 7]  = (*th1.temp)[i][j];
            humidity[i + 1][j + 11] = (*te1.temp)[i][j];
            humidity[i + 1][j + 15] = (*un1.temp)[i][j];

            if(_p.thousands == 0)
                pressure[i + 1][j + 6]  = 0;
            else
                pressure[i + 1][j + 6]  = (*th2.temp)[i][j];
            pressure[i + 1][j + 10] = (*te2.temp)[i][j];
            pressure[i + 1][j + 14] = (*un2.temp)[i][j];

            if(_aq.thousands == 0)
                air_quality[i + 1][j + 11]  = 0;
            else
                air_quality[i + 1][j + 11]  = (*th3.temp)[i][j];
            air_quality[i + 1][j + 15] = (*te3.temp)[i][j];
            air_quality[i + 1][j + 19] = (*un3.temp)[i][j];

            get_lum_bar(ss->_light);
        }
    }
}

void screen_update()
{
    var un4 = lookup_switch(_sc);
    for (uint8_t i = 0; i < 5; i++) {
        for (uint8_t j = 0; j < 3; j++) {
            temperature[i + 1][j + 26] = (*un4.temp)[i][j];
            pressure[i + 1][j + 27] = (*un4.temp)[i][j];
        }
    }
}

#endif /* LOOKUP_H_ */
