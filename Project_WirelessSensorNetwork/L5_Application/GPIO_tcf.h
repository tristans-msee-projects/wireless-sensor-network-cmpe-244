//Tristan French ID:012499851
//Lab 1 GPIO LED and Switch/*
/* GPIO_tcf.h
 *
 *  Created on: Sep 3, 2018
 *      Author: reldn_000
 */

#include <stdio.h>

#ifndef GPIO_TCF_H_
#define GPIO_TCF_H_

class GPIO_tcf {
private:
    /**
     * port, pin and any other variables should be placed here.
     * NOTE: that the state of the pin should not be recorded here. The true
     *      source of that information is embedded in the hardware registers
     */
    uint8_t port = 0;
    uint8_t pin = 0;
public:
    /**
     * You should not modify any hardware registers at this point
     * You should store the port and pin using the constructor.
     *
     * @param {uint8_t} pin  - pin number between 0 and 32
     */
    GPIO_tcf(uint8_t port_num, uint8_t pin_num);
    /**
     * Should alter the hardware registers to set the pin as an input
     */
    void setAsInput();
    /**
     * Should alter the hardware registers to set the pin as an input
     */
    void setAsOutput();
    /**
     * Should alter the set the direction output or input depending on the input.
     *
     * @param {bool} output - true => output, false => set pin to input
     */
    void setDirection(bool output);
    /**
     * Should alter the hardware registers to set the pin as high
     */
    void setHigh();
    /**
     * Should alter the hardware registers to set the pin as low
     */
    void setLow();
    /**
     * Should alter the hardware registers to set the pin as low
     *
     * @param {bool} high - true => pin high, false => pin low
     */
    void set(bool high);
    /**
     * Should return the state of the pin (input or output, doesn't matter)
     *
     * @return {bool} level of pin high => true, low => false
     */
    void toggle();
    /**
     * Should toggle output of pin
     */
    bool getLevel();
    ~GPIO_tcf();
};

#endif /* GPIO_TCF_H_ */
