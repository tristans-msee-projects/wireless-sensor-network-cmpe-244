#include "stdio.h"
#include "LPC17xx.h"
#include "utilities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "tasks.hpp"
#include "examples/examples.hpp"
#include "printf_lib.h"
#include "rtc.h"

#include "storage.hpp"
#include "stdlib.h"
#include "string.h"

#include <lookup.hpp>
#include "GPIO_tcf.h"
#include "data.hpp"
#include "uart0_min.h"

#include "wireless.h"


void row_selection(uint8_t row);
void temp_color(uint8_t i, uint8_t j);
void ev_switch(uint8_t i, uint8_t j);

void vMatrixTask(void * p);
void vSwitcherTask(void * p);
void vMatrix_Value_Update_Task(void * p);
void vWireless_rx(void * p);
void vLoggerTask(void * p);

SemaphoreHandle_t xLogDataSemaphore;
SemaphoreHandle_t xScreenSwitchSemaphore;

//Matrix variables

GPIO_tcf clk(0, 0);
GPIO_tcf oe(0, 1);
GPIO_tcf lat(0, 29);

GPIO_tcf R1(0, 30);
GPIO_tcf G1(1, 23);
GPIO_tcf B1(1, 28);

GPIO_tcf R2(1, 29);
GPIO_tcf G2(1, 30);
GPIO_tcf B2(1, 31);

GPIO_tcf rA(1, 19);
GPIO_tcf rB(1, 20);
GPIO_tcf rC(1, 22);

uint8_t (*top)[8][32] = &temperature;
uint8_t (*bottom)[8][32] = &humidity;

uint8_t switcher = 1;
uint8_t screen = 1;
uint16_t auto_bright;


int main(void)
{
    /**
     * A few basic tasks for this bare-bone system :
     *      1.  Terminal task provides gateway to interact with the board through UART terminal.
     *      2.  Wireless task responsible to receive, retry, and handle mesh network.
     *
     */
    scheduler_add_task(new terminalTask(PRIORITY_HIGH));

    /* Consumes very little CPU, but need highest priority to handle mesh network ACKs */
    scheduler_add_task(new wirelessTask(PRIORITY_CRITICAL));



    rtc_init();
    xLogDataSemaphore = xSemaphoreCreateBinary();
    xScreenSwitchSemaphore = xSemaphoreCreateBinary();


    xTaskCreate(vMatrixTask, (const char*) "Task outputs data to the LED matrix", 1024, (void*) NULL, 1, NULL);
    xTaskCreate(vSwitcherTask, (const char*) "Task switches display data", 1024, (void*) NULL, 2, NULL);
    xTaskCreate(vMatrix_Value_Update_Task, (const char*) "Task updates the environmental variables", 1024, (void*) NULL, 2, NULL);
    scheduler_add_task(new wirelessTask(PRIORITY_CRITICAL));
    xTaskCreate(vWireless_rx,(const char*)"Receive sensor values from different nodes", 2048, NULL, 3, NULL);
    xTaskCreate(vLoggerTask,(const char*)"Log sensor data", 1024, NULL, 2, NULL);

    scheduler_start(); ///< This shouldn't return
    return -1;
}




void vWireless_rx(void *p)
{
    uint8_t missed_pkt_cnt[4] = { 0 };
    while(1)
    {
           //Initializing the packet parameters
           mesh_packet_t rx;
           int command = 1;
           const char max_hops = 0;
           uint8_t addr;


           //Send command to nodes for data transmission
           for (uint8_t i = 0; i<4; i++){
               wireless_send(i+1, mesh_pkt_nack, &command, sizeof(command), max_hops);
               if (wireless_get_rx_pkt(&rx, 1000)){
                   wireless_deform_pkt(&rx, 5, &s1[i], sizeof(s1[0]));
                   missed_pkt_cnt[i] = 0;
               }
               else {
                   if(missed_pkt_cnt[i] >= 2){
                       s1[i] = {0,0,0,0,0};
                   }
                   missed_pkt_cnt[i]++;
               }
           }



          vTaskDelay(1000);

    }
}


void vMatrix_Value_Update_Task(void * p)
{
    GPIO_tcf sw_0(1, 9);
    GPIO_tcf sw_1(1, 10);
    GPIO_tcf sw_2(1, 14);
    GPIO_tcf sw_3(1, 15);

    sw_0.setAsInput();
    sw_1.setAsInput();
    sw_2.setAsInput();
    sw_3.setAsInput();

//    uint8_t last_sw = 1;
    _sc = 1;
    screen_update();

    while(1){
        if(sw_0.getLevel()){
            if (_sc != 1){
                _sc = 1;
                screen_update();
            }
        }
        else if(sw_1.getLevel()){
            if (_sc != 2){
                _sc = 2;
                screen_update();
            }
        }
        else if(sw_2.getLevel()){
            if (_sc != 3){
                _sc = 3;
                screen_update();
            }
        }
        else if(sw_3.getLevel()){
            if (_sc != 4){
                _sc = 4;
                screen_update();
            }
        }
        switch (_sc){
            case 1:
                matrix_value_update(&s1[0]);
                break;
            case 2:
                matrix_value_update(&s1[1]);
                break;
            case 3:
                matrix_value_update(&s1[2]);
                break;
            case 4:
                matrix_value_update(&s1[3]);
                break;
        }
        vTaskDelay(200);
    }
}

//Task for displaying sensor values on the Matrix

void vMatrixTask(void * p)
{
    //Initialize task
    clk.setAsOutput();
    clk.setLow();
    oe.setAsOutput();
    oe.setHigh();
    lat.setAsOutput();
    lat.setLow();

    R1.setAsOutput();
    R1.setLow();
    G1.setAsOutput();
    G1.setLow();
    B1.setAsOutput();
    B1.setLow();

    R2.setAsOutput();
    R2.setLow();
    B2.setAsOutput();
    B2.setLow();
    G2.setAsOutput();
    G2.setLow();

    rA.setAsOutput();
    rA.setLow();
    rB.setAsOutput();
    rB.setLow();
    rC.setAsOutput();
    rC.setLow();
    bool debugBool = 0;
    while (1) {

        for (int i = 0; i < 8; i++) {
            lat.setLow();
            oe.setHigh();
            row_selection(i);

            for (int j = 0; j < 32; j++) {
                ev_switch(i, j);

                clk.setHigh();
                clk.setLow();
                lat.setHigh();
                lat.setLow();
                delay_us(11 - auto_bright);
            }
            lat.setHigh();
            oe.setLow();
            auto_bright = get_brightness()/10;
            delay_us(auto_bright + 1);
        }

        oe.setHigh();
        if(xSemaphoreTake(xScreenSwitchSemaphore,0)){
            xSemaphoreGive(xLogDataSemaphore);
        }
        vTaskDelay(1);
    }

}

//Task to switch between multiple screens

void vSwitcherTask(void * p)
{
    while (1) {
        if (switcher == 1) {
            top = &temperature;
            bottom = &humidity;
            switcher++;
        }
        else if (switcher == 2){
            top = &temp1;
            bottom = &temp2;
            switcher++;
        }
        else if (switcher == 3){
            top = &pressure;
            bottom = &air_quality;
            switcher++;
        }
        else if(switcher == 4){

            top = &lum1;
            bottom = &lum2;
            switcher=1;
            xSemaphoreGive(xScreenSwitchSemaphore);
        }
        vTaskDelay(4000);
    }
}

void vLoggerTask(void * p)
{
    rtc_t a = rtc_gettime();
    char data[128] = {0};
    char fileName[32] = {0};
    char *ds = &data[0];
    char *fs = &fileName[0];
    uint16_t logCount = 0;
    while(1)
    {
        if(xSemaphoreTake(xLogDataSemaphore, portMAX_DELAY))
        {
            if (logCount >= 0)
            {
                a = rtc_gettime();
                for(int i = 0; i<4; i++)
                {
                    sprintf(fs, "1:SensorData_%i%i%i.txt", a.day, a.month, a.year);
                    sprintf(ds,"%i/%i/%i  %i:%i:%i node %i \n air: %i, light: %i, temp: %i C, Press: %i Hum: %i\% \n",
                            a.day, a.month, a.year, a.hour, a.min, a.sec, i, s1[i]._air_q,s1[i]._light,
                            s1[i]._temp,s1[i]._psi,s1[i]._hum);
                    Storage::append(fs,ds,strlen(ds), 0);
                    logCount = 0;
                }
            }
            else
            {
                logCount++;
            }
        }
    }
}

//Function to select the row to input the data on that row

void row_selection(uint8_t row)
{
    switch (row) {
        case 0:
            rA.setLow();
            rB.setLow();
            rC.setLow();
            break;
        case 1:
            rA.setHigh();
            rB.setLow();
            rC.setLow();
            break;
        case 2:
            rA.setLow();
            rB.setHigh();
            rC.setLow();
            break;
        case 3:
            rA.setHigh();
            rB.setHigh();
            rC.setLow();
            break;
        case 4:
            rA.setLow();
            rB.setLow();
            rC.setHigh();
            break;
        case 5:
            rA.setHigh();
            rB.setLow();
            rC.setHigh();
            break;
        case 6:
            rA.setLow();
            rB.setHigh();
            rC.setHigh();
            break;
        case 7:
            rA.setHigh();
            rB.setHigh();
            rC.setHigh();
            break;
    }
}

//Set the display color based on the temperature
void temp_color(uint8_t i, uint8_t j)
{
    if (temp_celsius < 5) {
        if (switcher == 2) {
            R1.set((*top)[i][j]);
            B1.set((*top)[i][j]);
            G1.set((*top)[i][j]);
        }

        else if (switcher == 3) {
            R1.set((*top)[i][j]);
            B1.set((*top)[i][j]);
            G1.set((*top)[i][j]);

            R2.set((*bottom)[i][j]);
            B2.set((*bottom)[i][j]);
            G2.set((*bottom)[i][j]);
        }
    }
    else if (temp_celsius >= 5 && temp_celsius < 15) {
        if (switcher == 2) {
            B1.set((*top)[i][j]);
            R1.setLow();
            G1.setLow();
        }
        else if (switcher == 3) {
            R1.setLow();
            G1.setLow();
            B1.set((*top)[i][j]);
            R2.setLow();
            G2.setLow();
            B2.set((*bottom)[i][j]);
        }
    }

    else if (temp_celsius >= 15 && temp_celsius < 22) {
        if (switcher == 2) {
            R1.setLow();
            B1.set((*top)[i][j]);
            G1.set((*top)[i][j]);
        }
        else if (switcher == 3) {
            R1.setLow();
            B1.set((*top)[i][j]);
            G1.set((*top)[i][j]);
            R2.setLow();
            B2.set((*bottom)[i][j]);
            G2.set((*bottom)[i][j]);
        }
    }

    else if (temp_celsius >= 22 && temp_celsius < 27) {
        if (switcher == 2) {
            R1.set((*top)[i][j]);
            G1.set((*top)[i][j]);
            B1.setLow();
        }
        else if (switcher == 3) {
            R1.set((*top)[i][j]);
            G1.set((*top)[i][j]);
            B1.setLow();

            R2.set((*bottom)[i][j]);
            G2.set((*bottom)[i][j]);
            B2.setLow();
        }
    }

    else if (temp_celsius >= 27 && temp_celsius < 35) {
        if (switcher == 2) {
            R1.set((*top)[i][j]);
            G1.setLow();
            B1.set((*top)[i][j]);
        }
        else if (switcher == 3) {
            R1.set((*top)[i][j]);
            G1.setLow();
            B1.set((*top)[i][j]);

            R2.set((*bottom)[i][j]);
            G2.setLow();
            B2.set((*bottom)[i][j]);
        }
    }

    else {
        if (switcher == 2) {
            R1.set((*top)[i][j]);
            B1.setLow();
            G1.setLow();
        }
        else if (switcher == 3) {
            B1.setLow();
            B2.setLow();
            G1.setLow();
            G2.setLow();
            R2.set((*bottom)[i][j]);
            R1.set((*top)[i][j]);
        }
    }
}

void ev_switch(uint8_t i, uint8_t j)
{
    switch (switcher) {
        case 2:
            temp_color(i, j);
            G2.set((*bottom)[i][j]);
            B2.set((*bottom)[i][j]);
            R2.setLow();
            break;
        case 3:
            temp_color(i, j);
            break;
        case 4:
            R1.set((*top)[i][j]);
            B1.setLow();
            G1.setLow();

            R2.set((*bottom)[i][j]);
            G2.set((*bottom)[i][j]);
            B2.set((*bottom)[i][j]);
            break;
        case 1:
            R1.set((*top)[i][j]);
            B1.set((*top)[i][j]);
            G1.set((*top)[i][j]);
            R2.set((*bottom)[i][j]);
            B2.set((*bottom)[i][j]);
            G2.set((*bottom)[i][j]);
            break;
    }
}

void setAsInput()
{
    LPC_PINCON->PINSEL2= 0x0000;
    LPC_GPIO1->FIODIR &= ~((1<<9) | (1<<10) | (1<<14) | (1<<15));
}
